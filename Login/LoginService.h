//
//  LoginService.h
//  Login
//
//  Created by 汪宁 on 2018/5/2.
//  Copyright © 2018年 WN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginService : NSObject
- (void) gotoLogin;
- (void) loginOut;
@end
